from flask import Flask, render_template, request
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
from projektR import *

app = Flask(__name__)

#app.secret_key = 'kisel1kesteN'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'wdip'

mysql = MySQL(app)

@app.route("/")
@app.route("/home")
def home():
	return render_template('index.html')

@app.route("/search", methods = ['GET', 'POST'])
def search():
	ip_address = request.form['ipinput']
	regex_ip_pattern = "^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$"
	if request.method == 'POST' and bool(re.match(regex_ip_pattern, ip_address)):
		#swag(ip_address)
		#sweep(ip_address)
		lista = []
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * from netinfo')
		for row in cursor:
			lista.append(row)
		cursor.close()
		return render_template('table.html', lista=lista)
	else:
		msg = ''
		msg = 'Unijeli ste netočan format IP adrese'
		return render_template('index.html', msg = msg)

@app.route("/edit", methods = ['GET', 'POST'])
def edit():
	#params = request.form.to_dict()
	params = request.form['mac']
	return render_template('details.html', params=params)

@app.route("/save", methods = ['GET', 'POST'])
def save():
	ip = request.form['ip']
	os = request.form['os']
	mac = request.form['mac']
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('UPDATE netinfo SET ip= % s, os = % s WHERE mac = % s', (ip, os, mac, ))
	mysql.connection.commit()

	lista = []
	cursor.execute('SELECT * from netinfo')
	for row in cursor:
		lista.append(row)
	cursor.close()
	return render_template('table.html', lista=lista)


def insertFour(mac, ip, prva_pojava, zadnja_pojava):
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('INSERT INTO netinfo VALUES (% s, % s, % s, % s, NULL, NULL, NULL)', (mac, ip, prva_pojava, zadnja_pojava, ))
	mysql.connection.commit()
	cursor.close()

def updateFourth(mac, ip, prva_pojava, zadnja_pojava):
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('UPDATE netinfo SET zadnja_pojava= % s WHERE mac = % s AND ip = % s AND prva_pojava = % s', (zadnja_pojava, mac, ip, prva_pojava, ))
	mysql.connection.commit()
	cursor.close()

def updateLastThree(mac, ip, prva_pojava, zadnji_scan, os, portinfo):
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('UPDATE netinfo SET OS= % s, portinfo= % s, zadnji_scan= % s WHERE mac = % s AND ip = % s AND prva_pojava = % s', (os, portinfo, zadnji_scan, mac, ip, prva_pojava, ))
	mysql.connection.commit()
	cursor.close()