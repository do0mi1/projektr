from scapy.all import *
from datetime import datetime
import csv
import pandas as pd
import nmap
from threading import Thread
from time import time

arp_info = {}  # Riječnik u koji ce se spremati parovi IP : MAC adresa na mreži
ip_list = []  # Lista u koju će se spremati IP adrese kojima je u datom trenutku pridružena neka MAC adresa
current = {}  # Riječnik u koji će se spremati parovi MAC adresa i ažuriranja trenutnog vremena njihovog pojavljivanja u mreži
first = {}  # Riječnik u koji će se spremati parovi MAC adresa i točnog vremena njihovog prvog pojavljivanja u mreži


def arp_sniff():  # Funkcija koja radi arp sniff cijelo vrijeme dok korisnik to ne prekine
    return sniff(filter="arp", store=0, prn=arp_collect)


def arp_collect(arp_pkg):  # Funkcija koju arp_sniff cijelo vrijeme izvršava, ona reagira na dolazak svakog arp paketa u mrežu
    mac = arp_pkg[ARP].hwsrc  # MAC adresa uređaja u mreži
    # IP adresa na koju se taj uređaj spojio u lokalnoj mreži
    ip = arp_pkg[ARP].psrc

    # Ako nema uređaja u mreži, spaja se i zauzima IP adresu u mreži
    if arp_info.get(mac) == None:
        arp_info[mac] = ip
        ip_list.append(ip)

        temp = datetime.now()
        temp = temp.strftime("%d-%m-%Y %H:%M:%S")
        first[mac] = temp
        current[mac] = temp

        insertFour(mac, ip, first.get(mac), current.get(mac))
        write_csv([mac, ip, first.get(mac), current.get(mac)])

    # Ako uređaj odjednom promjeni IP adresu u mreži, ovako se program može od toga oporaviti i pvoezati MAC adresu uređaja s tom IP adresom
    elif ip != arp_info[mac]:
        ip_list.remove(arp_info[mac])
        arp_info[mac] = ip
        ip_list.append(ip)

        temp = datetime.now()  #Datum prvog čitanja pridruživanja MAC i IP adrese
        temp = temp.strftime("%d-%m-%Y %H:%M:%S")
        first[mac] = temp
        current[mac] = temp
        #Zapis u CSV datoteku, zapisuju se MAC adresa i IP adresa kojoj je pridružena, 
        #prvo vrijeme pridruživanja MAC i IP i trenutno vrijeme koje se osvježava ako je MAC adresa i dalje pridružena istoj IP adresi
        write_csv([mac, ip, first.get(mac), current.get(mac)])  
        insertFour(mac, ip, first.get(mac), current.get(mac))

    print(ip_list)  #Periodički ispis svih uređaja u mreži u terminal, kontrolni ispis
    
    #Provjeravamo za svaki uređaj koji je pri prošlom ispitivanju bio u mreži je li i dalje u mreži
    for ip in ip_list: 
        HOST_UP = True if os.system("fping --quiet -r 2 " + ip) == 0 else False  #Provjeravamo je li IP aktivan

        #Ako je IP aktivan, dobijemo MAC adresu preko IP adrese i osvježimo zadnju pojavu uređaja u mreži
        if(HOST_UP):
            macc = list(arp_info.keys())[list(arp_info.values()).index(ip)]
            temp = datetime.now()
            temp = temp.strftime("%d-%m-%Y %H:%M:%S")
            current[macc] = temp

            #Putem modula pandas osvježavamo CSV datoteku na mjestu na kojem se nalazi MAC adresa tog uređaja, njegov pridruženi IP i vrijeme pridruživanja
            df = pd.read_csv('NetScan.csv')
            m = (df['mac'] == macc) & (df['ip'] == ip) & (
                df['prva_pojava'] == first[macc])
            df.loc[m, "zadnja_pojava"] = current[macc]
            df.to_csv("NetScan.csv", index=False)
            updateFourth(macc, ip, first[macc], current[macc])
        
        #Ako je IP neaktivan, mičemo uređaj kojem je bio pridružen iz liste aktivnih uređaja i iz svih rječnika (arp_info, first, current)
        else:
            ip_list.remove(ip)
            macc = list(arp_info.keys())[list(arp_info.values()).index(ip)]
            del first[macc]
            del current[macc]
            del arp_info[macc]

#Funkcija koja zapisuje podatke u CSV datoteku
def write_csv(data):

    #Zaglavlja u CSV datoteci
    headers = ['mac', 'ip', 'prva_pojava', 'zadnja_pojava',
               'zadnji_sken', 'portinfo', 'OS']        

    #Otvaramo datoteku i dodajemo zapise u nju
    with open('NetScan.csv', 'a') as outfile:
        writer = csv.writer(outfile)
        #Provjeravamo je li prazna datoteka, ako je, zapisujemo prvo zaglavlja
        if(is_empty_csv('NetScan.csv')):
            writer.writerow(headers)

        writer.writerow(data)

#Provjeravamo je li CSV datoteka prazna
def is_empty_csv(name):
    with open(name) as csvfile:
        reader = csv.reader(csvfile)
        for i, _ in enumerate(reader):
            if i:
                return False
    return True

#Funkcija koja obavlja aktivno skeniranje, nju poziva funkcija sweep malo kasnije
def scanner(nm, val, scandate): #Prima tri parametra, nmap koji skenira portove, IP adresa ili opseg njih koji gledamo i vrijeme skeniranja
    portinfo_list = [] #Lista informacija o portovima 
    print("START")  #Početak aktivnog skeniranja

    #U ovom try-catch bloku provjeravamo može li se prepoznati operacijski sustav uređaja,
    #a neki uređaji s npr. većom razinom zaštite i promjenjivom vanjskom MAC adresom mogu nekada ne dopuštati dijeljenje podataka pa i to može baciti iznimku
    try:
        HOST_UP2 = True if os.system(
            "fping --quiet -r 2 " + val) == 0 else False   #Provjeravamo je li IP adresa aktivna i ako je, pokrećemo sken
        if(HOST_UP2):
            #Prije skena napravimo sigurnosni ping scan kako bi nmap prepoznao IP i stavio ga u listu IP adresa, brzo je gotovo, a pomogne ako se uređaji ne otkrivaju brzo
            for i in range(10): 
                safety=nm.scan(val,arguments='-sP')  

            #Pokrećemo portscan koji provjerava može li se saznati operacijski sustav uređaja, limitira provjeru na one koji pokazuju znakove toga da bi mogli imati OS koji se može detektirati,
            #A ako ga ne može detektirati sa sigurnošću, može ga pokušati pogoditi i vratiti taj pogodak i podatak u obliku postotka koliko je siguran u njega    
            machine = nm.scan(
                val, arguments='-O --osscan-limit --osscan-guess -v -sV --open -T5')
            
            #Određivanje svih informacija vezanih uz OS uređaja
            machineOS = machine['scan'][val]['osmatch'][0]['osclass'][0]['osfamily']+' ' + \
                machine['scan'][val]['osmatch'][0]['osclass'][0]['osgen'] + \
                ' ('+machine['scan'][val]['osmatch'][0]['osclass'][0]['accuracy']+'%)'
        else:
            print("Ne postoji uređaj spojen na IP u mreži") #Ako HOST_UP2 vrati False, IP nije u mreži pa vraćamo to
    
    except Exception:
        #Ako dobijemo iznimklu na nm.scan-u, onda je moguće ne možemo prepoznati OS ili da uređaj trenutno ne dopušta dijeljenje podataka o njemu
        machineOS = "Operacijski sustav nije moguce prepoznati/Uredaj trenutno ne dopusta dijeljenje podataka" 
    
    try:
        open_ports = 0 #Kontrolna varijabla koja će provjeravati ima li otvorenih portova
        if(HOST_UP2):
            #Provjeravamo ima li otvorenih portova
            try:
                skenirani_ip = machine['scan'][val]
                open_ports = 1 
            except:
                portinfo_list.append(
                    "Nema otvorenih portova/Uredaj trenutno ne dopusta dijeljenje podataka")

            #Ako ima otvorenih portova, skenirati ćemo ih
            if(open_ports):
                protokoli = skenirani_ip.all_protocols() #Svi protokoli koji su pronađeni (tcp, udp)
                print(protokoli) #Ispis protokola, kontrolna varijabla

                #Za svaki protokol pokrećemo dublji sken
                for protokol in protokoli:
                    skenirani_ip_protokol = skenirani_ip[protokol] #trenutni protokol koji obrađujemo
                    portlist = list(skenirani_ip_protokol.keys()) #lista u koju spremamo portove koji koriste odabrani protokol (npr. 53, 80...)

                    #Za svaki port u listi provjeravamo podatke o imenu i verziji protokola
                    for port in portlist:
                        #Tri varijable u koje ćemo unositi podatke o određenom portu
                        name = ''
                        version = ''
                        extrainfo = ''
                        print(port) #Ispis porta, kontrolna varijabla

                        portname = skenirani_ip_protokol[port]['name'] #Dohvaćanje imena određenog porta i ispis za kontrolu
                        print(portname) 
                        protocol_version = '' #String u koji ćemo ubaciti sve informacije o verziji protokola

                        #Provjeravamo ima li port određene atribute vezane uz verziju i ubacujemo ih u protocol_version
                        if(skenirani_ip_protokol[port]['product']):
                            name = skenirani_ip_protokol[port]['product']
                            protocol_version += name
                        
                        if(skenirani_ip_protokol[port]['version']):
                            version = skenirani_ip_protokol[port]['version']
                            protocol_version += ' '+version
                        
                        if(skenirani_ip_protokol[port]['extrainfo']):
                            extrainfo = '(' + \
                                skenirani_ip_protokol[port]['extrainfo']+')'
                            protocol_version += ' '+extrainfo
                        
                        if(protocol_version == ''):
                            protocol_version = "No version found"

                        #Sve informacije o portu spremamo u string portinfo
                        portinfo = "PORT: " + \
                            str(port)+'/'+protokol+',  '+"NAME: " + \
                            portname+',  '+"VERSION: "+protocol_version
                        
                        #Na kraju portinfo dodajemo u listu koja sadrži portinfo o svim portovima nekog uređaja
                        portinfo_list.append(portinfo)
    
    #Ako uhvatimo iznimku, moguće je da nema otvorenih portova ili da uređaj ne dopušta dijeljenje podataka u ovom trenutku            
    except Exception:
        portinfo_list.append(
            "Nema otvorenih portova/Uređaj trenutno ne dopušta dijeljenje podataka")
    
    #Na kraju gledamo je li uređaj u mreži i ako je, zapisujemo sve informacije koje smo dobili
    #U međuvremenu je moguće da uređaj više nije u mreži zbog trajanja nmap skena pa se možemo osigurati s HOST_UP3
    HOST_UP3 = True if os.system(
            "fping --quiet -r 2 " + val) == 0 else False 
    if(HOST_UP3):
        #Listu portinfo_list pretvaramo u string i vrijednosti odvajamo s ||
        portinfo_str = ' || '.join(portinfo_list)
        
        #Isto kao s OS, provjeravamo postoji li MAC adresa spojena na IP koji provjeravamo, ako postoji, dodajemo sve podatke na točno mjesto gdje trebaju ići u CSV datoteci
        macc2 = list(arp_info.keys())[list(arp_info.values()).index(val)]
        df2 = pd.read_csv('NetScan.csv')
        m2 = (df2['mac'] == macc2) & (df2['ip'] == val) & (
            df2['prva_pojava'] == first[macc2])
        df2.loc[m2, "zadnji_sken"] = scandate
        df2.loc[m2, "OS"] = machineOS
        df2.loc[m2, "portinfo"] = portinfo_str
        df2.to_csv("NetScan.csv", index=False)
        updateLastThree(macc2, val, first[macc2], scandate, machineOS, portinfo_str)

#Funkcija koja pokreće aktivan sken
def sweep(ip):
    while True: #Aktivna je konstantno i čeka početak aktivnog skeniranja
            print("SWEEP")
            scandate = datetime.now()  #Vrijeme skeniranja
            scandate = scandate.strftime("%d-%m-%Y %H:%M:%S")
            nm = nmap.PortScanner() #Funkcija koja obavlja skeniranje portova
            val = ip  #Vrijednost koju skeniramo, čitamo ju s web sučelja
            if('/' not in val):  #Gledamo je li IP adresa pojedinačna 
                scanner(nm, val, scandate) #Pozivamo funkciju scanner
                print("SWEEP DONE")

            elif('/' in val): #Ako nije pojedinačna, provjeravamo je li ona opseg određen maskom
                response = os.popen(f"fping -aq -r 2 -g {val}").read() 
                lista = list(filter(None, response.split("\n"))) #Čitamo sve aktivne IP adrese u zadanom rasponu i zapisujemo ih u listu za kasniju iteraciju
                print("Aktivni IP-evi")
                print(lista) #Ispis aktivnih IP adresa, kontrolna varijabla
                for val in lista:
                    scanner(nm, val, scandate) #Pozivamo funkciju scanner za svaki aktivni IP u rasponu
                print("SWEEP DONE")

def swag(ip):
    val = ip
    nm = nmap.PortScanner()
    if('/' in val):
        response = os.popen(f"fping -aq -r 2 -g {val}").read()
        # Čitamo sve aktivne IP adrese u zadanom rasponu i zapisujemo ih u listu za kasniju iteraciju
        lista = list(filter(None, response.split("\n")))
        print("Aktivni IP-evi")
        print(lista)  # Ispis aktivnih IP adresa, kontrolna varijabla
        for val in lista:
            HOST_UP2 = True if os.system(
                "fping --quiet -r 2 " + val) == 0 else False  # Provjeravamo je li IP adresa aktivna i ako je, pokrećemo sken
            if(HOST_UP2):
                # Prije skena napravimo sigurnosni ping scan kako bi nmap prepoznao IP i stavio ga u listu IP adresa, brzo je gotovo, a pomogne ako se uređaji ne otkrivaju brzo
                for i in range(10):
                    safety = nm.scan(val, arguments='-sP')
        print("DONE")
    else:
        HOST_UP2 = True if os.system(
            "fping --quiet -r 2 " + val) == 0 else False  # Provjeravamo je li IP adresa aktivna i ako je, pokrećemo sken
        if(HOST_UP2):
            # Prije skena napravimo sigurnosni ping scan kako bi nmap prepoznao IP i stavio ga u listu IP adresa, brzo je gotovo, a pomogne ako se uređaji ne otkrivaju brzo
            for i in range(10):
                safety = nm.scan(val, arguments='-sP')

#Ako se main pokrene eksplicitno nad ovom datotekom, pokrenut će se dvije dretve, jedna će pasivno provjeravati ARP pakete koji dolaze u mrežu kad se uređaji spajaju
#Druga će čekati signal za aktivno pretraživanje mreže
if __name__ == '__main__':
    from app import *
    Thread(target=arp_sniff).start()
    #Thread(target=sweep).start()
